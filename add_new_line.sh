#!/usr/bin/env bash

###################################################################
#Script Name	: Add new line
#Description	:
#Args         : -h <Help>
#Author       :Pedram Sarani
#Email        :Saranipedram@yahoo.com
#Help		      :<Script_name> -h
#Version      : 0.1
###################################################################
print_help() {
  echo "Usage: $0 PATH"
  echo "You need to add Network Manager path if is different with "/etc/NetworkManager/NetworkManager.conf""
}

FILE_PATH=${1:-/etc/NetworkManager/NetworkManager.conf}


# Check Sudo User
if [[ $(id -u) -ne 0 ]]; then
  echo "Are you kidding me $USER?."
  echo -e "Please Chande the User to ${RED}root${NC} with ${Green}sudo${NC} command"
  exit 1
fi

#parse the flags
optsting=":h"
while getopts ${optsting} options; do
      case ${options} in
      	h)
  	      print_help
	        exit 0
          ;;
	     ?)
	       echo "Invalid option: -${OPTARG}"
 	       exit 1
	        ;;
     esac
done

if [[ -f ${FILE_PATH} ]]; then
  sed -i -e '/dns/d' -e 's/\[main\]/\[main\]\ndns=none/g' ${FILE_PATH}
  exit 0
else
  echo "File dosent exist!!"
  exit 1
fi
